import React, { Component } from 'react';
import './App.css';
import Title from './Component/Title';
import Search from './Component/Search';
import Sort from './Component/Sort';
import Form from './Component/Form';
import Item from './Component/Item';
import ItemEdit from './Component/ItemEdit';
import ItemList from './Component/ItemList';
 import SweetAlert from "sweetalert2-react";
import uuidv4 from 'uuid/v4'
import { orderBy as orderById } from 'lodash';
import TodoService from './services/TodoService';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      showAlert: false,
      titleAlert: '',
      idAlert: '',
      indexEdit: 0,
      idEdit: '',
      nameEdit: '',
      levelEdit: 0,
      arrayLevel: [],
      showForm: false,
      valueItem: '',
      levelItem: 0, 
      sortType: '',
      sortOrder: ''
    }
  };

  componentDidMount() {
    TodoService.getTodoList().then(todoList => {
      let arrayLevel = [];
        if (todoList.length > 0) {
          for (let i = 0; i < todoList.length; i++) {
            if (arrayLevel.indexOf(todoList[i].level) === -1) {
              arrayLevel.push(todoList[i].level);
            }
          }
        }
        //Sắp xếp mảng có giá trị từ bé đến lớn
        arrayLevel.sort(function (a, b) { return (a - b) });
        this.setState({
          items: todoList,
          arrayLevel: arrayLevel
        })
    })
  };

  handleShowAlert = (items) => {
    this.setState({
      ...this.state,
      showAlert: true,
      titleAlert: items.name,
      idAlert: items.id
    })
  };

  handleDeleteItem = (itemD) => {
    let { items } = this.state;
    if (items.length > 0) {
      for (let i = 0; i < items.length; i++) {
        if (items[i].id === itemD.id) {
          items.splice(i, 1);
          break;
        }
      };
    }
    let itemId= itemD.id;
    TodoService.deleteTodo(itemId).then(res=>{
      console.log(res);
      this.setState({
        items: items,
        showAlert: false
      });
    })
    
  };

  handleEditItem = (index, item) => {
    this.setState({
      indexEdit: index,
      idEdit: item.id,
      nameEdit: item.name,
      levelEdit: item.level
    });
  };

  handleEditClickCancel = () => {
    this.setState({
      idEdit: '',
      nameEdit: '',
      levelEdit: 0,
    })
  };

  handleEditInputChange = (value) => {
    this.setState({
      nameEdit: value
    });
  };

  handelEditSelectChange = (value) => {
    this.setState({
      levelEdit: value
    });
  };

  handleEditClickSubmit = () => {
    let { items, idEdit, nameEdit, levelEdit } = this.state;
    if (items.length > 0) {
      for (let i = 0; i < items.length; i++) {
        if (items[i].id === idEdit) {
          items[i].name = nameEdit;
          items[i].level = +levelEdit;
          break;
        }
      }
    }
    let data = {
      name: nameEdit,
      level: +levelEdit
    }
    TodoService.updateTodo(idEdit, data).then(res=>{
      console.log(res);
      this.setState({
        idEdit: ''
      });
    })

  };
  handleShowForm = () => {
    this.setState({
      showForm: !this.state.showForm
    });
  };

  handleFromInputChange = (value) => {
    this.setState({
      valueItem: value
    });
  };

  handleFormSelectChange = (value) => {
    this.setState({
      levelItem: value
    })
  };

  handleFormClickCancel = () => {
    this.setState({
      valueItem: '',
      levelItem: 0
    })
  };

  handleFormClickSubmit = () => {
    let { valueItem, levelItem } = this.state;
    if (valueItem.trim() === 0) return false;
    let newItem = {
      id: uuidv4(),
      name: valueItem,
      level: +levelItem
    }
    // items.push(newItem);

    TodoService.createNewTodo(newItem).then (res=>{
      console.log(res);
      this.setState({
        items: [...this.state.items, newItem]
      });
    })
    
    
  };

  handleSort = (sortType, sortOrder) => {
    this.setState({
      sortType: sortType,
      sortOrder: sortOrder
    });
    let { items } = this.state;
    this.setState({
      items: orderById(items, [sortType], [sortOrder])
    })
  }

  renderItem = () => {
    let { items, idEdit, indexEdit, nameEdit, levelEdit, arrayLevel } = this.state;
    if (items.length === 0) {
      return <Item item={0} />
    }
    return items.map((item, index) => {
      if (item.id === idEdit) {
        return (
          <ItemEdit
            key={index}
            indexEdit={indexEdit}
            nameEdit={nameEdit}
            levelEdit={levelEdit}
            arrayLevel={arrayLevel}
            handleEditClickCancel={this.handleEditClickCancel}
            handleEditInputChange={this.handleEditInputChange}
            handelEditSelectChange={this.handelEditSelectChange}
            handleEditClickSubmit={this.handleEditClickSubmit}
          />
        )
      }
      return (
        <Item
          item={item}
          index={index}
          handleShowAlert={this.handleShowAlert}
          handleDeleteItem={this.handleDeleteItem}
          handleEditItem={this.handleEditItem}
        />
      )
    });
  }

  render() {
    return (
      <div className="container">
        <SweetAlert
          show={this.state.showAlert}
          title="Xác nhận xóa"
          text={this.state.titleAlert}
          // showCancelButton
          // onOutsideClick={() => this.setState({ showAlert: false })}
          // onEscapeKey={() => this.setState({ showAlert: false })}
          // onCancel={() => this.setState({ ...this.state.items,showAlert: false })}
          onConfirm={() => this.handleDeleteItem()}
        />
        <Title></Title>
        <div className="row">
          <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <Search></Search>
          </div>
          <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <Sort
              sortType={this.state.sortType}
              sortOrder={this.state.sortOrder}
              handleSort={this.handleSort}
            />
          </div>
          <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
            <button type="button" className="btn btn-info btn-block marginB10"
              onClick={this.handleShowForm}>
              {(this.state.showForm) ? 'Close Item' : 'Add Item'}
            </button>
          </div>
        </div>
        <div className="row marginB10">
          <div className="col-md-offset-7 col-md-5">
            <Form
              showForm={this.state.showForm}
              arrayLevel={this.state.arrayLevel}
              valueItem={this.state.valueItem}
              handleFromInputChange={this.handleFromInputChange}
              levelItem={this.state.levelItem}
              handleFormSelectChange={this.handleFormSelectChange}
              handleFormClickCancel={this.handleFormClickCancel}
              handleFormClickSubmit={this.handleFormClickSubmit}
            />
          </div>
        </div>
        <ItemList handleShowAlert={this.handleShowAlert}
          handleDeleteItem={this.handleDeleteItem}
          handleEditItem={this.handleEditItem}
          renderItem={this.renderItem} />
      </div>
    );
  }
}

export default App;
