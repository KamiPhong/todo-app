import React, { Component } from 'react';

export class Form extends Component {
  constructor(props){
    super(props);
    this.state={
      
    }
  }

 

  renderLevel = () => {
    let { arrayLevel } = this.props;
    return arrayLevel.map((level, index) => {
      switch (level) {
        case 0:
          return <option key={index} value={level}>Thấp</option>
        case 1:
          return <option key={index} value={level}>Trung bình</option>
        default:
          return <option key={index} value={level}>Cao</option>
      }
    })
  };

  render() {
    if (this.props.showForm === false) return null;
    return (
      <form className="form-inline" onSubmit={()=>this.props.handleFormClickSubmit()}>
        <div className="form-group">
          <input type="text" className="form-control margin-5px" placeholder="Item Name"
            value={this.props.valueItem}
            onChange={(event) => {this.props.handleFromInputChange(event.target.value)}}
          />
        </div>
        <div className="form-group margin-5px">
          <select className="form-control"
            value={this.props.levelItem}
            onChange={(event) => this.props.handleFormSelectChange(event.target.value)}>
            {this.renderLevel()}
          </select>
        </div>
        <button type="button" className="btn btn-primary margin-5px"
          onClick={()=>this.props.handleFormClickSubmit()}>Submit</button>
        <button type="button" className="btn btn-default"
          onClick={() => {this.props.handleFormClickCancel()}}>Cancel</button>
      </form>
    )
  }
}

export default Form
