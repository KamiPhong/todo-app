import React, { Component } from 'react'
import Items from '../MockData/Items';
import './Item.css'
export class Item extends Component {

  constructor(props) {
    super(props);
    this.state = {
      active: "label-info",
      items: Items,
      showAlert: false
    }
  }

  componentDidMount() {
    //fetch data lays api
    if (this.props.item.level === 1) {
      this.setState({ ...this.state, active: "label-warning" });
    }
    if (this.props.item.level === 2) {
      this.setState({ ...this.state, active: "label-danger" });
    }
  }

  level = (item) => {
    if (item.level === 0) {
      return "thấp";
    }
    if (item.level === 1) {
      return "trung bình";
    }
    if (item.level === 2) {
      return "cao";
    }
  }

  render() {
    let { item, index } = this.props;
    if (item === 0) {
      return (
        <tr>
          <td colSpan="4" className="text-center">
            <h4>No Item</h4>
          </td>
        </tr>
      )
    }
    return (
      <tr>
        <td className="text-center">{index}</td>
        <td>{item.name}</td>

        <td className="text-center vertical-align">
          <div className="test"><span className={`label ${this.state.active}`}>{this.level(item)}</span></div>

        </td>

        <td>
          <button type="button" className="btn btn-warning btn-sm margin-5px" onClick={() => this.props.handleEditItem(index, item)}>Edit</button>
          <button type="button" className="btn btn-danger btn-sm" onClick={() => this.props.handleDeleteItem(item)}>Delete</button>
        </td>
      </tr>
    )
  }
}

export default Item