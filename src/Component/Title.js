import React, { Component } from 'react'

export class Title extends Component {
    render() {
        return (
            <div className="page-header">
                <h1>KamiPro _P1 - Kami List <small>ReactJS </small></h1>
            </div>
        );
    }
}

export default Title
