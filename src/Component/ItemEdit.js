import React, { Component } from 'react'

export class ItemEdit extends Component {
  renderLevel = () => {
    const { arrayLevel } = this.props;
    return arrayLevel.map((level, index) => {
      switch (level) {
        case 0:
          return <option key={index} value={level}>Thấp</option>
        case 1:
          return <option key={index} value={level}>Trung bình</option>
        default:
          return <option key={index} value={level}>Cao</option>
      }
    });
  }
  render() {
    return (
      <tr>
        <td className="text-center">{this.props.indexEdit}</td>
        <td><textarea type="text" className="form-control"
          value={this.props.nameEdit}
          onChange={(event) => this.props.handleEditInputChange(event.target.value)} /></td>
        <td className="text-center">
          <select className="form-control"
            value={this.props.levelEdit}
            onChange={(event) => this.props.handelEditSelectChange(event.target.value)}>
            {this.renderLevel()}
          </select>
        </td>
        <td>
          <button type="button" className="btn btn-default btn-sm margin-5px" onClick={() => this.props.handleEditClickCancel()}>Cancel</button>
          <button type="button" className="btn btn-success btn-sm" onClick={() => this.props.handleEditClickSubmit()}>Save</button>
        </td>
      </tr>
    )
  }
}

export default ItemEdit
