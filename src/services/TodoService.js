function getTodoList() {
  return fetch(`http://localhost:3000/todos`,
    {
      method: 'GET'
    })
    .then(response => response.json())
    .then(data => {
      return data;
    })
}

function createNewTodo(newItem) {
  return fetch("http://localhost:3000/todos", {
    method: "POST",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(newItem)
  })
    .then(response => response.json())
    .then(data => {
      return data;
    })
}

function deleteTodo(itemId) {
  return fetch(`http://localhost:3000/todos/${itemId}`, {
    method: "DELETE",
  })
    .then(response => response.json())
}

function updateTodo(itemId, data) {
  return fetch(`http://localhost:3000/todos/${itemId}`, {
    method: "PUT",
    headers: {
      'Content-type': 'application/json; charset=UTF-8', // Indicates the content 
    },
    body: JSON.stringify(data)
  })
    .then(response => response.json())
    .catch(err => console.log(err))
}

export default {
  getTodoList,
  createNewTodo,
  deleteTodo,
  updateTodo
}